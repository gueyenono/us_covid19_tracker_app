# DO NOT RUN AGAIN IF IMAGE FILES ARE ALREADY DOWNLOADED


pacman::p_load(
  rvest,
  dplyr,
  purrr,
  here,
  stringr.plus
)

url <- "https://www.states101.com/flags"

html <- read_html(url)


# Scrape state names
state_names <- html %>%
  html_nodes(css = ".col-md-3 a > b") %>%
  html_text()

# Scrape urls to the main pages of each state
state_urls <- html %>%
  html_nodes(css = ".col-md-3 a") %>%
  html_attr(name = "href") %>%
  paste0("https://www.states101.com", .) %>%
  unique()

state_names <- basename(state_urls)

# Get url pattern for flag jpg files from the main page of Alabama

pattern <- read_html(state_urls[1]) %>%
  html_node(css = ".img-thumbnail") %>%
  html_attr(name = "src") %>%
  paste0("https://www.states101.com/", .)

img_urls <- paste0(dirname(pattern), "/", state_names, ".svg")

# Create directory for image files
dir.create(here("inst/app/www/img/flags"))

# Download images
walk2(img_urls,
      state_names,
      ~ download.file(url = .x, destfile = paste0("inst/app/www/img/flags/", .y, ".svg")))
