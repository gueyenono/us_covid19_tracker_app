document.addEventListener("DOMContentLoaded", () => {

  // // Function for reloading the page
  // reloadPage = () => {
  //   window.location.reload()
  // }
  // setTimeout(reloadPage, 500)

  // Receive data from R Shiny
  // Shiny.addCustomMessageHandler("shoutout", function(obj){
  //   console.log("This is a shoutout to " + obj.name + "!")
  // })

  // Get data from Shiny
  // $(document).on("shiny:sessioninitialized", function(){
  //   Shiny.addCustomMessageHandler("data_to_js", function(obj){
  //     covid = JSON.parse(obj.json)
  //   })
  // })

  // Functions

  // Function for finding matching state names
  function findMatches(nameToMatch, states) {
    const regex = new RegExp(nameToMatch, "gi")
    return states.filter(state => {
      return state.Province_State.match(regex)
    })
  }

  // Function for displaying the matching state names
  function displayMatches() {
    const matchedStates = findMatches(stateSelector.value, covid)
    let html = matchedStates.map(state => {
      if(stateSelector.value === ""){
        return;
      } else {
        return `
          <li>${state.Province_State}</li>
        `
      }
    }).join("")
    suggestions.innerHTML = html
  }

  // Variables
  const endpoint = "https://covid2019-api.herokuapp.com/v2/current/US"

  // Get page elements
  const dateDisplay = document.querySelector(".current-date")
  const stateSelector = document.querySelector("#state-selector")
  const stateName = document.querySelector(".state-name")
  const suggestions = document.querySelector(".suggestions")

  // Get COVID19 data from API
  let covid = []
  let states = []

  $(document).on("shiny:sessioninitialized", function(){
    fetch(endpoint)
      .then(promise => promise.json())
      .then(x => covid.push(...x.data))
      .then(Shiny.setInputValue("covid_data", covid))
  })

  // Display current date in header
  let currentDate = new Date()
  dateDisplay.innerHTML = currentDate.toDateString()

  // Display state name and flag
  stateSelector.addEventListener("keyup", () => {
    Shiny.setInputValue("state", stateSelector.value)
    stateName.innerHTML = stateSelector.value
    displayMatches()
  })

})
